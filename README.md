# BookTrack

BookTrack is a Django web application built to track recently read books and view the trends in reading patterns.

### Features

- Track and manage recently read books
- View trends in reading pattern
-  Google Books API integration to make the process of searching and adding new books a breeze

### Setting up

The following steps will get a local copy of the project running on local server:

#### Prerequisites

Install and set up python3, pip and virtualenv in the local machine.

#### Installation

Git clone the project into the local machine. cd into the projects folder.

```
virtualenv env
env\Scripts\activate
pip install -r requirements.txt
```

Then, copy the contents of the `.env.example` file into `.env` file. Create database according to the configurations in the `.env` file. After creating the database, migrate the models.

```
python manage.py makemigrations
python manage.py migrate
```

### Running

Finally, the project is ready to run. Run it using the command:

`python manage.py runserver`

This will start the Django application in localhost at port 8000. Access the application at `localhost:8000`.
 
