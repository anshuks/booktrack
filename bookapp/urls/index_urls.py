from django.urls import path

from bookapp.views import index_views as views

urlpatterns = [
    path('', views.index, name='index'),
    path('home/', views.home, name='home'),
    path('trends/', views.trends, name='trends'),
]
