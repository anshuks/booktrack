from django.urls import path

from bookapp.views import book_views as views

urlpatterns = [
    path('add-book/', views.add_book, name='add_book'),
    path('edit-book/<int:pk>/', views.edit_book, name='edit_book'),
    path('delete-book/<int:pk>/', views.delete_book, name='delete_book'),
    path('bookshelf/', views.bookshelf, name='bookshelf'),
    path('api-add/', views.api_add, name='api_add'),
]
