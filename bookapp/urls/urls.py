from bookapp.urls import book_urls, registration_urls, index_urls

urlpatterns = index_urls.urlpatterns
urlpatterns += registration_urls.urlpatterns
urlpatterns += book_urls.urlpatterns
