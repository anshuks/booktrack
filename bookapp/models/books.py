from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class Books(models.Model):
    name = models.CharField(max_length=200)
    author = models.CharField(max_length=300)
    genre = models.CharField(max_length=50, blank=True)
    rating = models.IntegerField(default=1)
    comment = models.TextField(blank=True)
    read_date = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
