from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils import timezone

from bookapp.models import UserProfile


def user_login(request):
    if request.user.is_authenticated:
        return redirect('home')

    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return redirect('home')
            else:
                return redirect('login')
        else:
            messages.error(request, 'These credentials do not match our records.')
            return redirect('login')

    return render(request, 'bookapp/registration/login.html')


def user_signup(request):
    if request.user.is_authenticated:
        return redirect('home')

    if request.method == "POST":
        try:
            try:
                user = User(
                    first_name=request.POST.get('fname'),
                    last_name=request.POST.get('lname'),
                    email=request.POST.get('email'),
                    date_joined=timezone.now(),
                    last_login=timezone.now(),
                    username=request.POST.get('username'),
                )
                user.set_password(request.POST.get('password'))
                user.save()
            except Exception as error:
                return HttpResponse(error)

            user_profile, _ = UserProfile.objects.get_or_create(user=user)
            user_profile.gender = request.POST.get('gender')
            user_profile.save()
            messages.success(request, "Login to continue")
            return redirect('login')
        except Exception as error:
            messages.error(request, "Signup failed. Please try again.")
            return render(request, 'bookapp/registration/signup.html')

    return render(request, 'bookapp/registration/signup.html')


@login_required
def user_logout(request):
    logout(request)
    return redirect('index')
