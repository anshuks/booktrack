import pandas as pd
import requests
from django.contrib.auth.decorators import login_required
from django.db.models import Count
from django.http import JsonResponse
from django.shortcuts import render, redirect

from bookapp.models import Books
from booktrack.settings import API_URL, KEY


def index(request):
    if request.user.is_authenticated:
        return redirect('home')

    return render(request, 'bookapp/index.html')


@login_required
def home(request):
    headers = {'key': KEY}
    if request.method == 'POST':
        query = request.POST.get('query')
        payload = {'q': query, 'maxResults': '20', 'country': 'US'}
        r = requests.get(API_URL, headers=headers, params=payload)
        return JsonResponse(r.json())

    return render(request, 'bookapp/home.html')


@login_required
def trends(request):
    try:
        genre_book = Books.objects.exclude(genre__iexact='N/A').exclude(genre__iexact='').filter(
            user=request.user).values('genre').annotate(Count('pk'))
        pf = pd.DataFrame(list(genre_book))
        genre = list(pf['genre'])
        genre_count = list(pf['pk__count'])

        author_book = Books.objects.exclude(author__iexact='N/A').exclude(author__iexact='').filter(
            user=request.user).values('author').annotate(Count('pk'))
        pg = pd.DataFrame(list(author_book))
        author = list(pg['author'])
        author_count = list(pg['pk__count'])

        rate_book = Books.objects.filter(user=request.user).values('rating').annotate(Count('pk'))
        ph = pd.DataFrame(list(rate_book))
        rate = list(ph['rating'])
        rate = [str(rate) + " *" for rate in rate]
        rate_count = list(ph['pk__count'])

        return render(request, 'bookapp/trends.html',
                      {'genre': genre, 'genre_count': genre_count, 'author': author, 'author_count': author_count,
                       'rate': rate, 'rate_count': rate_count})
    except Exception:
        error = "No Books"
        return render(request, 'bookapp/trends.html',
                      {'error': error})
