from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt

from bookapp.models import Books


@login_required
def add_book(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        author = request.POST.get('author')
        genre = request.POST.get('genre')
        rating = request.POST.get('rate')
        comment = request.POST.get('comment')
        if request.POST.get('date'):
            read_date = request.POST.get('date')
        else:
            read_date = timezone.now()
        Books.objects.create(name=name, author=author, genre=genre, rating=rating, comment=comment, read_date=read_date,
                             user=request.user)

        messages.success(request, "Book added to bookshelf")
        return redirect('bookshelf')

    return render(request, 'bookapp/add_books.html')


@login_required
def edit_book(request, pk):
    book = get_object_or_404(Books, pk=pk)
    if request.method == 'POST':
        book.name = request.POST.get('name')
        book.author = request.POST.get('author')
        book.genre = request.POST.get('genre')
        book.rating = request.POST.get('rate')
        book.comment = request.POST.get('comment')
        if request.POST.get('date'):
            book.read_date = request.POST.get('date')
        else:
            book.read_date = timezone.now()
        book.save()

        messages.success(request, "Book edited")
        return redirect('bookshelf')

    return render(request, 'bookapp/edit_books.html', {'book': book})


@login_required
def bookshelf(request):
    books = Books.objects.filter(user=request.user).order_by('read_date')
    return render(request, 'bookapp/bookshelf.html', {'books': books})


@login_required
def delete_book(request, pk):
    book = Books.objects.get(pk=pk)
    book.delete()
    messages.success(request, "Book deleted from bookshelf")
    return HttpResponseRedirect(reverse('bookshelf'))


@csrf_exempt
@login_required
def api_add(request):
    if request.method == 'POST':
        title = request.POST.get('title')
        author = request.POST.get('author')
        genre = request.POST.get('genre')
        Books.objects.create(name=title, author=author, genre=genre, user=request.user)
        messages.success(request, "Book added to bookshelf")
        return redirect('bookshelf')
